from flask import Flask, url_for, redirect, render_template

app = Flask(__name__)

@app.route('/')
@app.route('/home', methods=['GET', 'POST'])
def home():
    return render_template('home.html')

@app.route('/video', methods=['GET', 'POST'])
def video():
    #Returns Video Template
    return render_template('video.html', title='Our Video')

@app.route('/diagrams', methods=['GET', 'POST'])
def diagrams():
    return render_template('diagrams.html', title='Diagrams')

if __name__ == "__main__":
    app.run()